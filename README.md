# GitLab Group Access Tokens Report

This Go script is designed to enhance token management and rotation by producing a CSV report that details all group and project access tokens associated with a specified GitLab group and its subgroups. The report includes valuable information such as token age, scopes, expiration, access level and other relevant details.


## Video Walkthrough: https://tinyurl.com/2achs5hj

## Requirements

- Go 1.20 or later
- GitLab private token
- GitLab group id

## Usage
1. Set the `API_URL`, `GROUP_ID` and `PRIVATE_TOKEN` environment variables:

```bash
export API_URL=https://gitlab.com/api/v4
export PRIVATE_TOKEN=YOUR_PRIVATE_TOKEN
export GROUP_ID=YOUR_GROUP_ID
```

Replace `YOUR_PRIVATE_TOKEN` with your actual GitLab private token.

This will generate a `report.csv` file in the current directory with the access tokens information.

## CSV Report

The CSV report includes the following columns:

- Type
- Name
- ID
- Token ID
- Token Name
- Scopes
- Created At
- Expires At
- Access Level
- Active
- Revoked

## Notes

- The `API_URL`, `GROUP_ID,`, and `PRIVATE_TOKEN` environment variables are required for the script to run. Make sure to set them before running the script.
- You need to provide the ID of the top-level group you want to generate the report for.
- The script fetches project and group access tokens for the given group and all its subgroups and projects.
