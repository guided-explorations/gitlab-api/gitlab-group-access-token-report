package main

import (
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
)

type Group struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type AccessToken struct {
    ID          int      `json:"id"`
    Name        string   `json:"name"`
    Scopes      []string `json:"scopes"`
    CreatedAt   string   `json:"created_at"`
    ExpiresAt   string   `json:"expires_at"`
    AccessLevel int      `json:"access_level"`
    Active      bool     `json:"active"`
    Revoked     bool     `json:"revoked"`
}

func httpGetAndParse(url string, target interface{}) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	return json.Unmarshal(body, target)
}

func getSubgroups(gitlabAPIURL, privateToken, groupID string) ([]Group, error) {
	var subgroups []Group
	err := httpGetAndParse(fmt.Sprintf("%s/groups/%s/subgroups?private_token=%s", gitlabAPIURL, groupID, privateToken), &subgroups)
	return subgroups, err
}

func getProjects(gitlabAPIURL, privateToken, groupID string) ([]Group, error) {
	var projects []Group
	err := httpGetAndParse(fmt.Sprintf("%s/groups/%s/projects?private_token=%s", gitlabAPIURL, groupID, privateToken), &projects)
	return projects, err
}

func getGroupDetails(gitlabAPIURL, privateToken, groupID string) (Group, error) {
    var group Group
    url := fmt.Sprintf("%s/groups/%s?private_token=%s", gitlabAPIURL, groupID, privateToken)
    err := httpGetAndParse(url, &group)
    return group, err
}

func processEntity(gitlabAPIURL, privateToken string, entity Group, entityType string, csvWriter *csv.Writer) error {
    var url string
    if entityType == "group" {
        url = fmt.Sprintf("%s/groups/%d/access_tokens?private_token=%s", gitlabAPIURL, entity.ID, privateToken)
    } else if entityType == "project" {
        url = fmt.Sprintf("%s/projects/%d/access_tokens?private_token=%s", gitlabAPIURL, entity.ID, privateToken)
    } else {
        return fmt.Errorf("invalid entity type: %s", entityType)
    }

    var tokens []AccessToken
    err := httpGetAndParse(url, &tokens)
    if err != nil {
        return fmt.Errorf("Error processing %s %d: %v", entityType, entity.ID, err)
    }

    for _, token := range tokens {
        record := []string{
            strings.Title(entityType),
            entity.Name,
            strconv.Itoa(entity.ID),
            strconv.Itoa(token.ID),
            token.Name,
            strings.Join(token.Scopes, ", "),
            token.CreatedAt,
            token.ExpiresAt,
            strconv.Itoa(token.AccessLevel),
            strconv.FormatBool(token.Active),
            strconv.FormatBool(token.Revoked),
        }
        if err := csvWriter.Write(record); err != nil {
            return fmt.Errorf("Error writing to CSV for %s %d: %v", entityType, entity.ID, err)
        }
    }
    return nil
}


func processGroupandSubgroups(gitlabAPIURL, privateToken string, groupIDInt int, csvWriter *csv.Writer) error {
	currentGroup, err := getGroupDetails(gitlabAPIURL, privateToken, strconv.Itoa(groupIDInt))
	if err != nil {
		return fmt.Errorf("Error getting current group %d: %v", groupIDInt, err)
	}

	err = processEntity(gitlabAPIURL, privateToken, currentGroup, "group", csvWriter)
	if err != nil {
		return err
	}

	projects, err := getProjects(gitlabAPIURL, privateToken, strconv.Itoa(groupIDInt))
	if err != nil {
		return fmt.Errorf("Error getting projects for group %d: %v", groupIDInt, err)
	}

	for _, project := range projects {
		err := processEntity(gitlabAPIURL, privateToken, project, "project", csvWriter)
		if err != nil {
			return err
		}
	}

	subgroups, err := getSubgroups(gitlabAPIURL, privateToken, strconv.Itoa(groupIDInt))
	if err != nil {
		return fmt.Errorf("Error getting subgroups for group %d: %v", groupIDInt, err)
	}

	for _, subgroup := range subgroups {
		err := processGroupandSubgroups(gitlabAPIURL, privateToken, subgroup.ID, csvWriter)
		if err != nil {
			return err
		}
	}

	return nil
}
  
func main() {
	gitlabAPIURL := os.Getenv("API_URL")
	privateToken := os.Getenv("PRIVATE_TOKEN")
	groupID := strings.TrimSpace(os.Getenv("GROUP_ID"))

	if groupID == "" {
		fmt.Println("Please provide a top-level group ID using the GROUP_ID environment variable.")
		return
	}

	groupIDInt, err := strconv.Atoi(groupID)
	if err != nil {
		fmt.Println("Error converting group ID to integer:", err)
		return
	}
	
	csvFile, err := os.Create("report.csv")
	if err != nil {
		fmt.Println("Error creating CSV file:", err)
		return
	}
	defer csvFile.Close()

	csvWriter := csv.NewWriter(csvFile)
	defer csvWriter.Flush()

	csvWriter.Write([]string{"Type", "Name", "ID", "Token ID", "Token Name", "Scopes", "Created At", "Expires At", "Access Level", "Active", "Revoked"})

	err = processGroupandSubgroups(gitlabAPIURL, privateToken, groupIDInt, csvWriter)
	if err!= nil {
		fmt.Println("Error processing group:", err)
		return
	}
}
